package main

import "fmt"

type action struct {
	State    int
	Inp      int
	NewState int
	Action   int
}

type automat struct {
	states           map[int]*action
	raw              byteString
	x, y, st, v, eat int
	log              []int
}

func newAutomat(aut byteString) *automat {
	auto := &automat{
		states: make(map[int]*action),
		y:      31,
		log:    make([]int, 0),
		raw:    aut,
	}
	for i := 0; i < 4; i++ {
		for j := 0; j < 2; j++ {
			spos := (i*2 + j) * 4
			auto.states[i*10+j] = &action{
				State:    i,
				Inp:      j,
				NewState: byteToInt(aut[spos : spos+2]),
				Action:   byteToInt(aut[spos+2 : spos+4]),
			}
		}
	}
	return auto
}
func (a *automat) ls() string {
	res := ""
	for _, e := range a.log {
		if e == 0 {
			res += "S"
		}
		if e == 1 {
			res += "L"
		}
		if e == 2 {
			res += "R"
		}
		if e == 3 {
			res += "F"
		}
	}
	return res
}
func (a *automat) String() string {
	res := ""
	res = res + fmt.Sprintln(a.raw)
	res = res + fmt.Sprintln(a.eat)
	res = res + fmt.Sprintln(a.ls())
	return res
}
func (a *automat) moveV(dd int) bool {
	a.log = append(a.log, 3)
	if a.v == 0 {
		return a.move(dd, 0)
	}
	if a.v == 1 {
		return a.move(0, -dd)
	}
	if a.v == 2 {
		return a.move(-dd, 0)
	}
	if a.v == 3 {
		return a.move(0, dd)
	}
	return true
}
func (a *automat) move(dx, dy int) bool {
	x := a.x + dx
	y := a.y + dy
	if (x < 0) || (x > 31) || (y < 0) || (y > 31) {
		return false
	}
	a.x = x
	a.y = y
	return true
}

func (a *automat) appleCount(m amap) int {
	if a.v == 0 {
		return m.right(a.x, a.y)
	}
	if a.v == 1 {
		return m.down(a.x, a.y)
	}
	if a.v == 2 {
		return m.left(a.x, a.y)
	}
	if a.v == 3 {
		return m.up(a.x, a.y)
	}
	return 0
}

func (a *automat) hasApple(m amap) int {
	if a.appleCount(m) > 0 {
		return 1
	}
	return 0
}

func (a *automat) left() {
	a.log = append(a.log, 1)
	a.v = (a.v + 1) % 4
}

func (a *automat) right() {
	a.log = append(a.log, 2)
	a.v = (a.v + 3) % 4
}

func (a *automat) modeling() int {
	m := appleMap.copy()
	for step := 0; step < maxStep; step++ {
		inp := a.hasApple(m)
		act := a.states[a.st*10+inp]
		if act.Action == 1 {
			a.left()
		}
		if act.Action == 2 {
			a.right()
		}
		if act.Action == 3 {
			a.moveV(1)
		}
		if m[a.x][a.y] > 0 {
			a.eat++
			m[a.x][a.y]--
		}
		a.st = act.NewState
	}
	return a.eat
}
