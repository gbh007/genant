package main

import (
	"flag"
	"io"
	"log"
	"os"
	"path"
	"path/filepath"
)

var (
	mutGen         = false
	maxStep        = 200
	simCount int64 = 10000000
	port           = "8080"
)

func main() {
	wr := flag.Bool("w", false, "run web viewer")
	mG := flag.Bool("mg", false, "mutate chance on gen")
	pl := flag.String("pl", "", "parse log")
	lim := flag.Int64("lim", -1, "max generate count")
	cou := flag.Int64("step", -1, "max step count")
	flag.StringVar(&port, "port", "8080", "web port")
	flag.Parse()
	if *pl != "" {
		parseLog(*pl)
		return
	}
	mutGen = *mG
	if *cou > 0 {
		simCount = *cou
	}
	if *wr {
		web()
	} else {
		gen(*lim)
	}
}

func parseLog(pl string) {
	fileNames, err := filepath.Glob(
		path.Join(
			pl,
			"info.txt",
		))
	if err != nil {
		log.Fatalln(err)
	}
	res, err := os.Create(path.Join("log", "info.txt"))
	defer res.Close()
	if err != nil {
		log.Fatalln(err)
	}
	for _, name := range fileNames {
		file, err := os.Open(name)
		if err != nil {
			log.Println(err)
			continue
		}
		io.Copy(res, file)
	}
}
