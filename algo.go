package main

import (
	"bytes"
	"crypto/sha256"
	"fmt"
	"io"
	"math/rand"
	"sort"
	"sync"
)

type algo struct {
	pop          []*automat
	uniq         map[string]bool
	step         int64
	lastHash     [32]byte
	lastUniqStep int64
	mutChance    float64
}

func newAlgo() *algo {
	return &algo{
		pop:  make([]*automat, 0),
		uniq: make(map[string]bool),
	}
}

func (a *algo) Len() int           { return len(a.pop) }
func (a *algo) Less(i, j int) bool { return a.pop[i].eat > a.pop[j].eat }
func (a *algo) Swap(i, j int)      { a.pop[i], a.pop[j] = a.pop[j], a.pop[i] }
func (a *algo) best() *automat {
	sort.Sort(a)
	return a.pop[0]
}

func (a *algo) addAuto(auto ...*automat) {
	for _, e := range auto {
		if _, ok := a.uniq[e.raw.String()]; !ok {
			a.pop = append(a.pop, e)
			a.uniq[e.raw.String()] = true
		}
	}
}

func (a *algo) next() {
	sort.Sort(a)
	if len(a.pop) > 500 {
		a.pop = a.pop[:500]
	}
	par1 := a.pop[0]
	par2 := a.pop[1]
	par3 := a.randAutomat()
	par4 := a.randAutomat()
	chi1 := newAutomat(trax(par1.raw, par2.raw))
	chi2 := newAutomat(trax(par1.raw, par3.raw))
	chi3 := newAutomat(trax(par3.raw, par4.raw))
	chi1.raw = muta(chi1.raw, a.mutChance)
	chi2.raw = muta(chi2.raw, a.mutChance)
	chi3.raw = muta(chi3.raw, a.mutChance)
	wg := &sync.WaitGroup{}
	wg.Add(3)
	go func() {
		chi1.modeling()
		wg.Done()
	}()
	go func() {
		chi2.modeling()
		wg.Done()
	}()
	go func() {
		chi3.modeling()
		wg.Done()
	}()
	wg.Wait()
	a.addAuto(chi1, chi2, chi3)
	a.step++
}

func (a *algo) dump(w io.Writer) {
	buf := &bytes.Buffer{}
	sort.Sort(a)
	for i, e := range a.pop {
		if i >= 50 {
			break
		}
		fmt.Fprintln(buf, e.eat, e.raw)
	}
	if tmp := sha256.Sum256(buf.Bytes()); tmp != a.lastHash {
		a.lastHash = tmp
		a.lastUniqStep = a.step
		fmt.Fprintln(w, "-------------------------------------\nstep", a.step, "\n-------------------------------------")
		buf.WriteTo(w)
	}
}

func (a *algo) randAutomat() *automat {
	return a.pop[rand.Intn(len(a.pop))]
}

func trax(a, b byteString) byteString {
	res := byteString{}
	for i := 0; i < len(a); i++ {
		if rand.Intn(2) > 0 {
			res[i] = a[i]
		} else {
			res[i] = b[i]
		}
	}
	return res
}

func muta(a byteString, munCh float64) byteString {
	res := byteString{}
	if mutGen {
		for i, e := range a {
			res[i] = e
			if rand.Float64() < munCh {
				res[i] = (e + 1) % 2
			}
		}
	} else {
		if rand.Float64() < munCh {
			for i, e := range a {
				res[i] = e
			}
			ind := rand.Intn(len(res))
			res[ind] = (res[ind] + 1) % 2
		}
	}
	return res
}
