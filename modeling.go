package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"
)

var appleMap = newMap()

func byteToInt(b []byte) int {
	return int(b[0]*2 + b[1])
}

type byteString [32]byte

func newBS() byteString {
	bs := byteString{}
	for i := 0; i < 32; i++ {
		bs[i] = byte(rand.Intn(2))
	}
	return bs
}

func (bs byteString) String() string {
	res := ""
	for _, e := range bs {
		if e > 0 {
			res += "1"
		} else {
			res += "0"
		}
	}
	return res
}

func gen(lim int64) {
	file, _ := os.OpenFile("log/info.txt", os.O_APPEND|os.O_WRONLY|os.O_CREATE, 0777)
	defer file.Close()
	if lim > 0 {
		var i int64
		for ; i < lim; i++ {
			out := mod()
			fmt.Fprintln(file, out)
		}
	} else {
		for {
			out := mod()
			fmt.Fprintln(file, out)
		}
	}
}
func mod() string {
	log.Println("new")
	seed := time.Now().Unix()
	rand.Seed(seed)
	al := newAlgo()
	al.mutChance = rand.Float64()
	if mutGen {
		al.mutChance /= 32.0
	}
	tm := time.Now()
	fname := fmt.Sprintf(
		"log/%d_%s_log.txt",
		int(al.mutChance*100),
		tm.Format("20060102_150405"),
	)
	file, _ := os.Create(fname)
	defer file.Close()
	fmt.Fprintln(file, "seed", seed)
	fmt.Fprintln(file, "mutation chance", al.mutChance)
	if mutGen {
		fmt.Fprintln(file, "mutation type gen")
	} else {
		fmt.Fprintln(file, "mutation type individ")
	}
	fmt.Fprintln(file, "max step", maxStep)
	fmt.Fprintln(file, appleMap)
	ini := []*automat{
		newAutomat(newBS()),
		newAutomat(newBS()),
		newAutomat(newBS()),
		newAutomat(newBS()),
		//newAutomat(byteString{0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 0}),
	}
	for _, e := range ini {
		e.modeling()
	}
	al.addAuto(ini...)
	var i int64
	for i < simCount {
		if i%1000 == 0 {
			log.Printf("%d ( %d ) / %d : %d", i, al.lastUniqStep, simCount, al.best().eat)
			al.dump(file)
		}
		al.next()
		i++
	}
	al.dump(file)
	fmt.Fprintln(file, "----------------------------------------")
	fmt.Fprintln(file, "unique", len(al.uniq))
	fmt.Fprintln(file, "last step", al.step)
	return fmt.Sprintf(
		"max eat: %d mutation chance: %.4f uniq: %d last uniq step: %d steps: %d filename: %s",
		al.best().eat,
		al.mutChance,
		len(al.uniq),
		al.lastUniqStep,
		al.step,
		fname,
	)
}
