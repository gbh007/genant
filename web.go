package main

import (
	"encoding/json"
	"log"
	"net/http"
)

func parseJSON(r *http.Request, data interface{}) error {
	dec := json.NewDecoder(r.Body)
	err := dec.Decode(&data)
	if err != nil {
		log.Println(err)
	}
	return err
}
func writeJSON(w http.ResponseWriter, data interface{}, onlyheader bool) error {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS")
	w.Header().Set("Content-Type", "application/json")
	if onlyheader {
		w.Header().Set("Allow", "POST, OPTIONS")
		return nil
	}
	w.WriteHeader(http.StatusOK)
	enc := json.NewEncoder(w)
	err := enc.Encode(&data)
	if err != nil {
		log.Println(err)
	}
	return err
}

func api(w http.ResponseWriter, r *http.Request) {
	if r.Method == http.MethodOptions {
		writeJSON(w, nil, true)
		return
	}
	writeJSON(w, parseInfo(), false)
}

func web() {
	http.HandleFunc("/api", api)
	http.Handle("/", http.StripPrefix("/", http.FileServer(http.Dir("./static/"))))
	http.ListenAndServe(":"+port, nil)
}
