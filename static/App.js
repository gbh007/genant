var app = new Vue({
    el: '#app',
    data: {
        data:[],
        chart:null,
    },
    mounted:function(){
        this.data=[]
        let self = this
        axios.post("/api").then(function(response){
            response.data.forEach(e=>self.data.push(e))
        })
    },
    methods:{
        showMutEat:function(){
            let data=[]
            this.data.forEach(e=>data.push({x:e.mut,y:e.eat}))
            this.show(data,'вероятность мутации','съеденные яблоки')
        },
        showMutUniq:function(){
            let data=[]
            this.data.forEach(e=>data.push({x:e.mut,y:e.uniq}))
            this.show(data,'вероятность мутации','уникальных особей')
        },
        showMutLastUniq:function(){
            let data=[]
            this.data.forEach(e=>data.push({x:e.mut,y:e.last}))
            this.show(data,'вероятность мутации','шаг с последней уникальной группой')
        },
        showUniqLastUniq:function(){
            let data=[]
            this.data.forEach(e=>data.push({x:e.uniq,y:e.last}))
            data.sort(function(a,b){
                if (a.x<b.x) {return -1}
                if (a.x>b.x) {return 1}
                return 0
            })
            this.show(data,'уникальных особей','шаг с последней уникальной группой')
        },
        show:function(data,xname,yname){
            if (this.chart==null){
                this.chart=Chart.Bubble(this.$refs.gr,{})
            }
            this.chart.options={
                title:{
                    display:false,
                },
                legend:{
                    display:false,
                },
                scales: {
                    xAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: xname,
                        },
                    }],
                    yAxes: [{
                        scaleLabel: {
                            display: true,
                            labelString: yname,
                        },
                    }],
                },
            },
            this.chart.data={
                datasets:[
                    {
                        label:"data",
                        borderColor:"blue",
                        data:data,
                    },
                ],
            }
            this.chart.update()
        },
        getUrl:function(){
            this.$refs.a.href=this.chart.toBase64Image()
            this.$refs.a.click()
        },
    },
})
