package main

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
)

// Info структура для представление строки информации в виде JSON объекта
type Info struct {
	Eat   int     `json:"eat"`
	Mut   float64 `json:"mut"`
	Uniq  int     `json:"uniq"`
	Last  int     `json:"last"`
	Steps int     `json:"steps"`
}

func parseInfo() []*Info {
	res := make([]*Info, 0)
	file, err := os.Open("log/info.txt")
	if err != nil {
		log.Println(err)
	}
	templ := regexp.MustCompile(`max eat: (\d+) mutation chance: ([\d\.]+) uniq: (\d+) last uniq step: (\d+) steps: (\d+) filename: .*`)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		raw := scanner.Text()
		if templ.MatchString(raw) {
			row := templ.FindStringSubmatch(raw)
			info := &Info{}
			info.Eat, _ = strconv.Atoi(row[1])
			info.Mut, _ = strconv.ParseFloat(row[2], 64)
			info.Uniq, _ = strconv.Atoi(row[3])
			info.Last, _ = strconv.Atoi(row[4])
			info.Steps, _ = strconv.Atoi(row[5])
			res = append(res, info)
		}
	}
	return res
}
