Приведем краткое описание задачи “Умный муравей”. Используется двумерный тор размером 32 на 32 клетки. На некоторых клетках поля расположены яблоки. Яблоки расположены вдоль некоторой ломаной линии, но не на всех ее клетках. Клетки ломаной, на которых их нет – серые. Белые клетки – не принадлежат ломаной и не содержат яблок. Всего на поле 89 яблок.  
В клетке с пометкой “Start” находится муравей. Он занимает клетку поля и смотрит в одном из четырех направлений (север, запад, юг, восток). В начале игры муравей смотрит на восток. Он умеет определять находится ли яблоко непосредственно перед ним. За один ход муравей совершает одно из четырех действий:
* идет вперед на одну клетку, съедая яблоко, если оно было перед ним;
* поворачивается вправо;
* поворачивается влево;
* стоит на месте.

Съеденные муравьем яблоки не восполняются. Муравей жив на всем протяжении игры – еда не является необходимым ресурсом для его существования. Никаких других персонажей, кроме муравья, на поле нет. Ломаная строго задана. Муравей может ходить по любым клеткам поля.  
Игра длится 200 ходов, на каждом из которых муравей совершает одно из четырех описанных выше действий. В конце игры подсчитывается количество яблок, съеденных муравьем. Это значение – результат игры.  
Цель игры – создать муравья, который за 200 ходов съест как можно больше яблок. Муравьи, съевшие одинаковое количество яблок, заканчивают игру с одинаковым результатом вне зависимости от числа ходов, затраченных каждым из них на процесс еды. Однако эта задача может иметь различные модификации, например, такую, в которой при одинаковом количестве съеденных яблок, лучшим считается муравей, съевший яблоки за меньшее число ходов. Ниже будет показано, что поведение муравья может быть задано конечным автоматом. При этом может быть поставлена задача о построении автомата с минимальным числом состояний для муравья, съедающего все яблоки, или автомата для муравья, съедающего максимальное количество яблок при заданном числе состояний.  
Ввиду фиксированной топологии и размера поля (тор размером 32 на 32), фиксированного расположения яблок и их невосполнимости, а также примитивности поведения муравья, может показаться, что задача тривиальна.  
Для  того  чтобы  убедиться,  что  это  не  так,  предлагается сконструировать муравья, который съест хотя бы 82 яблока за 200 ходов. Приведенное выше число 82 выбрано неслучайно, так как существует простая стратегия с результатом 81, которая описана ниже.  

В работе [1] приведен автомат, который решает рассматриваемую задачу за 200 ходов и содержит 13 состояний  
* N  – непосредственно перед муравьем нет еды;
* F – непосредственно перед муравьем есть еда;
* M – идти вперед;
* L – повернуть налево;
* R – повернуть направо;
* NO – стоять на месте.

Кодирование автомата, задающего поведение муравья, в особь генетического алгоритма (битовую строку) в этой работе осуществлялось следующим образом: входному воздействию F сопоставлялась единица, а N – ноль. Каждое из четырех действий муравья кодировалось двоичными числами: 00 – NO, 01 – L, 10 – R, 11 – M.
Покажем, как выполняется кодирование автомата.
Пример автомата, описывающего поведение муравья
Кодирование этого графа переходов приведено в таблице:

Состояние|Вход|Новое состояние|Действие
---|---|----|---
00 | 0 | 01 | 01
00 | 1 | 00 | 11
01 | 0 | 01 | 10
01 | 1 | 10 | 11
10 | 0 | 11 | 00
10 | 1 | 10 | 11
11 | 0 | 00 | 01
11 | 1 | 11 | 10

Преобразуем эту таблицу в битовую строку. Для этого сначала требуется запомнить число состояний автомата (в данном случае четыре). В начале строки задан двоичный номер начального состояния автомата (в данном примере 00). Далее записаны пары (Новое состояние, Действие).
Содержимое полей (Состояние, Вход) не записываются, так как они повторяются для каждого автомата с фиксированным числом состояний. Для рассматриваемого примера искомая строка имеет вид:
“0101 0011 0110 1011 1100 1011 0001 1110”.


направление движения
v | x | y
--|---|--
0 | + |  
1 |   | -
2 | - |  
3 |   | +
