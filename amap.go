package main

import "math/rand"

type amap [32][32]int

func newMap() amap {
	res := amap{}
	cou := 0
	for cou < 89 {
		x, y := rand.Intn(32), rand.Intn(32)
		if res[x][y] == 0 {
			res[x][y] = 1
			cou++
		}
	}
	return res
}

func (a amap) copy() amap {
	res := amap{}
	for i := 0; i < 32; i++ {
		for j := 0; j < 32; j++ {
			res[i][j] = a[i][j]
		}
	}
	return res
}

func (a amap) String() string {
	res := ""
	for j := 31; j > -1; j-- {
		for i := 0; i < 32; i++ {
			if a[i][j] == 0 {
				res += "."
			} else {
				res += "@"
			}
		}
		res += "\n"
	}
	return res
}

func (a amap) left(x, y int) (res int) {
	for i := 0; i <= x; i++ {
		if a[i][y] > 0 {
			res++
		}
	}
	return
}

func (a amap) right(x, y int) (res int) {
	for i := x; i < 32; i++ {
		if a[i][y] > 0 {
			res++
		}
	}
	return
}

func (a amap) up(x, y int) (res int) {
	for i := 0; i <= y; i++ {
		if a[x][i] > 0 {
			res++
		}
	}
	return
}

func (a amap) down(x, y int) (res int) {
	for i := y; i < 32; i++ {
		if a[x][i] > 0 {
			res++
		}
	}
	return
}
